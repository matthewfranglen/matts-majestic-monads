# pylint: disable=bad-continuation, no-member
import urllib.request  # would normally use requests
from argparse import ArgumentParser

from monads import Either


def main():
    result = (
        make_parser()
            .fmap(ArgumentParser.parse_args)
            .fmap(to_url)
            .bind(read)
            .fmap(len)
    )
    print(result)


@Either.convert
def make_parser():
    parser = ArgumentParser(description='Download a URL and print the length of the body')
    parser.add_argument('URL', help='URL to download')
    return parser


def to_url(arguments):
    return arguments.URL


@Either.convert
def read(url):
    with urllib.request.urlopen(url) as response:
        return response.read()


if __name__ == '__main__':
    main()
